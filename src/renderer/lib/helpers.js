import fs from 'fs'

export default {
    sleep: (pass) => {
        return new Promise((resolve) => {
            setTimeout(function () {
                resolve(pass)
            }, 15000)
        })
    },
    transformDate : (date) => {
        const [d, m, y] = date.split('.');
        return `${y}-${m}-${d}T00:00:00`;
    },
    capitalize: (string) => {
        // let string = String(arg);
        return string.charAt(0).toUpperCase() + string.slice(1);
    },

    logger: (level, message) => {
        // const timeStamp = new Date().toLocaleString();
        // const path = '/tmp/walkit/flytrap/log.txt';
        // fs.appendFileSync(path, `${timeStamp} ${level} ${message}\n`)
        new Notification(level,{body: message})
    }
}
