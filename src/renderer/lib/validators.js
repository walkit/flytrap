
let isCyrillicSymbols = (value) => {
    const re = /^[а-я]+$/i;
    return re.test(value);
};

let isNormalLength = (value, len) => {
    return value >= len;
};

let isBirthdayDate = (input) => {
    // const re = /^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$/g;
    const re = /^([0-2][0-9]|(3)[0-1])(\.)(((0)[0-9])|((1)[0-2]))(\.)\d{4}$/g;
    return re.test(input);
};

export default {
    validateForm: (biometry) => {
        return new Promise((resolve, reject) => {

            if (!isCyrillicSymbols(biometry.lastName) || isNormalLength(biometry.lastName,2)) {
                reject('lastName');
            }

            if (!isCyrillicSymbols(biometry.firstName) || isNormalLength(biometry.firstName,2)) {
                reject('firstName');
            }

            if (!isCyrillicSymbols(biometry.fatherName) || isNormalLength(biometry.fatherName,2)) {
                reject('fatherName');
            }

            if (!isBirthdayDate(biometry.birthday)) {
                reject('birthday')
            }

            if (biometry.phoneNumber.length !== 11) {
                reject('phoneNumber')
            }

            if (biometry.cardNumber.length !== 5) {
                reject('cardNumber')
            }

            resolve(biometry);
        })
    },
    validateSms: (validationPair) => {
        return new Promise((resolve, reject)=> {
            if (validationPair.clientCode === validationPair.generatedCode) {
                resolve('valid')
            } else {
                reject('smsCode')
            }
        })
    }
}
