const SMSru = require('sms_ru');
const apiID = '5FA918AB-0ECC-2915-C31C-B1E4D70397BA';
const sms = new SMSru(apiID);

export default {
    sendSms: (biometry) => {
        return new Promise((resolve)=> {
            sms.sms_send(
                {
                    to: biometry.phoneNumber,
                    text: `Сообщите код кассиру: ${biometry.validationCode}`,
                },
                (response) => {
                    // todo: обрабатывать статус коды
                    console.log(response);
                    resolve(response)
                }
            );
        })
    },

    checkStatus: (smsResponse) => {
        return new Promise((resolve)=> {
            sms.sms_status(smsResponse.ids, (status) => {
                console.log(status);
                new Notification('Статус сообщения', {body: status.description})
                resolve(status)
            })
        })
    }
}
