import queries from './queries';
import helpers from '../lib/helpers'


let convertInputData = (biometry, connection) => {
    return new Promise((resolve) => {
        let props = {
            fullName: `${biometry.lastName} ${biometry.firstName} ${biometry.fatherName}`,
            birthday: helpers.transformDate(biometry.birthday),
            phoneNumber: biometry.phoneNumber,
            cardNumber: biometry.cardNumber,
            dataBaseURL: connection.dataBaseURL,
            login: connection.login,
            password: connection.password
        };
        resolve(props)
    })
};

export default {
    writeBiometryToDatabase: (biometry, connection) => {
        return new Promise((resolve, reject) => {
            convertInputData(biometry, connection)
                .then(queries.createPartner)
                .then(queries.createContragent)
                .then(queries.createCard)
                .then(() => {
                    resolve('record done')
                })
                .catch(err => {
                    reject(err)
                });
        })

        // console.log(biometry);
        // console.log(connection)
    }
}
