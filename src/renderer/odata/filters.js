module.exports = {
  fieldEqualToString: (fieldName, query) => {
    return `&$filter=${fieldName} eq '${query}'`;
  },

  fieldSubstringof: (fieldName, substring) => {
    //
    return `$filter=substringof('${substring}', ${fieldName}) eq true`;
  }
};
