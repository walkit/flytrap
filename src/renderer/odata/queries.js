const c = require('./catalogs');
const f = require('./filters');
const axios = require('axios').default;

const format = '?&$format=json';

const log = require('electron-log');

export default {

    checkAuth(props) {
        return new Promise((resolve, reject) => {
            const url = encodeURI(`${props.dataBaseURL}${format}`);
            axios({
                method: 'get',
                url,
                auth: {
                    username: unescape(encodeURIComponent(props.login)),
                    password: unescape(encodeURIComponent(props.password))
                }
            })
                .then((res) => {
                    log.info('Авторизация:', res.status, res.statusText);
                    resolve(res);
                })
                .catch((err) => {
                    log.error('Авторизация:', err.message);
                    reject(err)
                })
        });
    },

    createPartner: (props) => {
        return new Promise((resolve, reject) => {
            const partnerData =
                {
                    'Description': props.fullName,
                    'Клиент': true,
                    'Комментарий': 'Контрагент создан в программе Walkit:FlyTrap',
                    'Поставщик': false,
                    'НаименованиеПолное': props.fullName,
                    'Конкурент': false,
                    'ПрочиеОтношения': false,
                    'ОбслуживаетсяТорговымиПредставителями': false,
                    'ДополнительнаяИнформация': '',
                    'Перевозчик': false,
                    'ЮрФизЛицо': 'ЧастноеЛицо',
                    'ДатаРождения': props.birthday,
                    'КонтактнаяИнформация': [
                        {
                            'LineNumber': '1',
                            'Тип': 'Телефон',
                            'Вид_Key': '1e4e36c7-745f-11e8-90b0-50465d584d3a',
                            'Представление': props.phoneNumber,
                            'НомерТелефона': props.phoneNumber
                        }
                    ]
                };
            const url = encodeURI(`${props.dataBaseURL}${c.Partners}${format}`);
            let data = JSON.stringify(partnerData);

            console.log(data);
            axios({
                method: 'post',
                url,
                auth: {
                    username: unescape(encodeURIComponent(props.login)),
                    password: unescape(encodeURIComponent(props.password))
                },
                data
            })
                .then(res => {
                    log.info('Создание партнера:', res.status, res.statusText);
                    props.partnerKey = res.data.Ref_Key;
                    resolve(props);
                })
                .catch(err => {
                    log.error('Создание партнера:', err.message);
                    return reject(err);
                });
        });
    },

    createContragent: (props) => {
        return new Promise((resolve, reject) => {
            const contragentData = {
                'ЮридическоеФизическоеЛицо': 'ФизическоеЛицо',
                'ЮрФизЛицо': 'ФизЛицо',
                'НаименованиеПолное': props.fullName,
                'Description': props.fullName,
                'Партнер_Key': props.partnerKey,
            };
            const url = encodeURI(`${props.dataBaseURL}${c.Contragents}${format}`);

            axios({
                method: 'post',
                url,
                auth: {
                    username: unescape(encodeURIComponent(props.login)),
                    password: unescape(encodeURIComponent(props.password))
                },
                data: JSON.stringify(contragentData)
            })
                .then(res => {
                    log.info('Создание контрагента:', res.status, res.statusText);
                    props.contragentKey = res.data.Ref_Key;
                    resolve(props);
                })
                .catch(err => {
                    log.error('Создание контрагента:', err.message);
                    return reject(err);
                });
        });
    },

    createCard: (props) => {
        return new Promise((resolve, reject) => {
            const cardData =
                {
                    "Owner_Key": "c483eac5-28ed-11e4-a8c0-d850e64e6c9d",
                    "Штрихкод": props.cardNumber,
                    "Партнер_Key": props.partnerKey,
                    "Description": `${props.fullName}. № карты: ${props.cardNumber}`
                };

            const url = encodeURI(`${props.dataBaseURL}${c.Cards}${format}`);

            axios({
                method: 'post',
                url,
                auth: {
                    username: unescape(encodeURIComponent(props.login)),
                    password: unescape(encodeURIComponent(props.password))
                },
                data: JSON.stringify(cardData)
            })
                .then(res => {
                    log.info('Создание карты лояльности:', res.status, res.statusText);
                    props.cardKey = res.data.Ref_Key;
                    resolve(props)
                })
                .catch(err => {
                    log.error('Создание карты лояльности:', err.message);
                    return reject(err);
                });
        });
    }
};
