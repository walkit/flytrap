import queries from '../../odata/queries'
import helpers from '../../lib/helpers'

const state = {
    inputFields: {
        lastName: {title: 'Фамилия', value: '', placeholder: 'Иванов', enabled: true},
        firstName: {title: 'Имя', value: '', placeholder: 'Иван', enabled: true},
        fatherName: {title: 'Отчество', value: '', placeholder: 'Иванович', enabled: true,},
        birthday: {title: 'Дата рождения', value: '', placeholder: 'ДД.ММ.ГГГГ', enabled: true,},
        phoneNumber: {title: 'Номер телефона', value: '', placeholder: '79991112233', enabled: true,},
        cardNumber: {title: 'Номер карты', value: '', placeholder: '00000', enabled: true,},
        smsCode: {title: 'Код проверки', value: '', placeholder: '0000', enabled: false,},
        loginField: {title: 'Логин', value: '', enabled: true,},
        passwordField: {title: 'Пароль', value: '', enabled: true,},
        domainField: {title: 'Домен', value: '', enabled: true, placeholder: 'https://database.com'}
    },
    buttons: {
        login: {title: 'Войти', enabled: true, isRedButton: false},
        reset: {title: 'СБРОС', enabled: true, isRedButton: true},
        print: {title: 'Печать анкеты', enabled: false, isRedButton: false},
        reprint: {title: 'Повтор печати', enabled: false, isRedButton: false},
        send: {title: 'Отправить код', enabled: true, isRedButton: false},
        resend: {title: 'Повтор отправки', enabled: false, isRedButton: true},
        checkSmsStatus: {title: 'Статус СМС', enabled: false, isRedButton: true},
        checkCodeFromSms: {title: 'Проверить код', enabled: false, isRedButton: false},
        write: {title: 'Записать в 1с', enabled: false, isRedButton: false}
    },
    messageBoxes: {
        login: {message: 'Авторизация'},
        newClient: {message: 'Заполни данные клиента'},
        error: {message: ''}
    },
    connection: {
        odata: '/odata/standard.odata/',
        dataBaseURL: '',
        format: '?&$format=json;odata=nometadata',
        login: '',
        password: '',
    },
    inputFieldValidationError: {
        hasError: false,
    },
    validationCode: '',
    smsResponse: undefined,
    splashIsEnabled: false,

};

const mutations = {
    updateInputFieldValue(state, payload) {
        state.inputFields[payload.id].value = payload.value
    },

    updateAuthData(state) {
        state.connection.login = state.inputFields.loginField.value;
        state.connection.password = state.inputFields.passwordField.value;
    },

    resetForm(state) {

        state.inputFields.lastName.value = '';
        state.inputFields.lastName.enabled = true;
        state.inputFields.firstName.value = '';
        state.inputFields.firstName.enabled = true;
        state.inputFields.fatherName.value = '';
        state.inputFields.fatherName.enabled = true;
        state.inputFields.birthday.value = '';
        state.inputFields.birthday.enabled = true;
        state.inputFields.phoneNumber.value = '';
        state.inputFields.phoneNumber.enabled = true;
        state.inputFields.cardNumber.value = '';
        state.inputFields.cardNumber.enabled = true;
        state.inputFields.smsCode.value = '';
        state.inputFields.smsCode.enabled = false;
        state.buttons.send.enabled = true;
        state.buttons.resend.enabled = false;
        state.buttons.checkSmsStatus.enabled = false;
        state.buttons.reprint.enabled = false;
        state.buttons.print.enabled = false;
        state.buttons.write.enabled = false;
        state.validationCode = '';
        state.inputFieldValidationError.hasError = false;
        state.smsResponse = undefined;
        state.splashIsEnabled = false;
    },

    resetConnection(state) {
        state.inputFields.loginField.value = '';
        state.inputFields.loginField.enabled = true;
        state.inputFields.passwordField.value = '';
        state.inputFields.passwordField.enabled = true;
        state.buttons.login.enabled = true;
        state.connection.dataBaseURL = ''
    },

    lockNewClientForm(state) {
        state.inputFields.lastName.enabled = false;
        state.inputFields.firstName.enabled = false;
        state.inputFields.fatherName.enabled = false;
        state.inputFields.birthday.enabled = false;
        state.inputFields.cardNumber.enabled = false;
        state.inputFields.smsCode.enabled = true;
        state.buttons.send.enabled = false;
        state.buttons.checkSmsStatus.enabled = true;
        state.buttons.checkCodeFromSms.enabled = true;
        state.buttons.resend.enabled = true;
    },

    lockLoginForm(state) {
        state.splashIsEnabled = true;
        // state.inputFields.loginField.enabled = false;
        // state.inputFields.passwordField.enabled = false;
        // state.inputFields.domainField.enabled = false;

        state.connection.dataBaseURL = state.inputFields.domainField.value + state.connection.odata
        // state.buttons.login.enabled = false;
    },
    unlockLoginForm(state) {
        state.splashIsEnabled = false;
        state.inputFields.loginField.enabled = true;
        state.inputFields.passwordField.enabled = true;
        state.inputFields.domainField.enabled = true;
        state.buttons.login.enabled = true;
    },

    updateValidationCode(state, code) {
        state.validationCode = code
    },

    updateInputFieldValidationError(state, error) {
        state.inputFieldValidationError.hasError = error.hasError;
        state.messageBoxes.error.message = error.errorText;
    },

    cleanError(state) {
        state.inputFieldValidationError.hasError = false;
        state.messageBoxes.error.message = '';
    },

    updateSmsResponse(state, smsResponse) {
        state.smsResponse = smsResponse;
    },

    lockNewClientBeforePrint(state) {
        state.inputFields.phoneNumber.enabled = false;
        state.inputFields.smsCode.enabled = false;
        state.buttons.checkSmsStatus.enabled = false;
        state.buttons.resend.enabled = false;
        state.buttons.checkCodeFromSms.enabled = false;
        state.buttons.print.enabled = true;
    },
    lockNewClientBeforeWrite(state) {
        state.buttons.print.enabled = false;
        state.buttons.checkCodeFromSms.enabled = false;
        state.buttons.checkSmsStatus.enabled = false;
        state.buttons.reprint.enabled = true;
        state.buttons.write.enabled = true;

    },

    capitalizeFIO(state) {
        state.inputFields.firstName.value = helpers.capitalize(state.inputFields.firstName.value);
        state.inputFields.lastName.value = helpers.capitalize(state.inputFields.lastName.value);
        state.inputFields.fatherName.value = helpers.capitalize(state.inputFields.fatherName.value)
    },
    changeSplashState(state, enabled) {
        state.splashIsEnabled = enabled
    }
};

const getters = {
    splashStatus: state => {
        return state.splashIsEnabled
    },

    biometryFormData: state => {
        return {
            lastName: state.inputFields.lastName.value,
            firstName: state.inputFields.firstName.value,
            fatherName: state.inputFields.fatherName.value,
            birthday: state.inputFields.birthday.value,
            phoneNumber: state.inputFields.phoneNumber.value,
            cardNumber: state.inputFields.cardNumber.value
        }
    },
    connectionData: state => {
        return state.connection
    },
    inputFieldById: state => {
        return (id) => state.inputFields[id]
    },
    buttonById: state => {
        return (id) => state.buttons[id]
    },
    messageBoxById: state => {
        return (id) => state.messageBoxes[id]
    },
    validationError: state => {
        return state.inputFieldValidationError.hasError
    },

    smsResponse: state => {
        return state.smsResponse
    },

    validationPair: state => {
        return {
            clientCode: state.inputFields.smsCode.value,
            generatedCode: state.validationCode
        }
    },


};

const actions = {
    doUpdateInputFieldValue({commit}, payload) {
        commit('updateInputFieldValue', payload)
    },

    doUpdateAuthData({commit, state}) {
        return new Promise((resolve, reject) => {
            queries.checkAuth({
                dataBaseURL: state.connection.dataBaseURL,
                login: state.inputFields.loginField.value,
                password: state.inputFields.passwordField.value
            }).then((res) => {
                // console.log(res);
                commit('updateAuthData');
                resolve(res)
            }).catch(err => reject(err))
        })

    },

    doResetForm({commit}) {
        commit('resetForm')
    },
    doResetConnection({commit}) {
        commit('resetConnection')
    },

    doLockNewClientForm({commit}, biometry) {
        return new Promise((resolve) => {
            commit('lockNewClientForm');
            resolve(biometry);
        });
    },

    doGenerateValidationCode({commit}, biometry) {
        return new Promise((resolve) => {
            biometry.validationCode = Math.floor(Math.random() * (9999 - 1000 + 1)) + 1000;
            commit('updateValidationCode', biometry.validationCode.toString());
            resolve(biometry)
        })
    },

    doUpdateInputFieldValidationError({commit, getters}, inputFieldId) {
        let inputFieldTitle = getters.inputFieldById(inputFieldId).title;
        let error = {
            hasError: true,
            errorText: `Поле '${inputFieldTitle}' заполнено не верно!`
        };
        commit('updateInputFieldValidationError', error)
    },
    doUpdateLoginError({commit}) {
        let error = {
            hasError: true,
            errorText: `Ошибка авторизации.`
        };
        commit('updateInputFieldValidationError', error)
    },

    doLockLoginForm({commit}) {
        commit('lockLoginForm')
    },
    doUnlockLoginForm({commit}) {
        commit('unlockLoginForm')
    },
    doCleanError({commit}) {
        commit('cleanError')
    },

    doUpdateSmsResponse({commit}, smsResponse) {
        return new Promise((resolve) => {
            commit('updateSmsResponse', smsResponse);
            resolve(smsResponse);
        })
    },

    doLockNewClientBeforePrint({commit}) {
        commit('lockNewClientBeforePrint')
    },
    doLockNewClientBeforeWrite({commit}) {
        commit('lockNewClientBeforeWrite')
    },
    doCapitalizeFIO({commit}) {
        commit('capitalizeFIO')
    },
    doShowSplashScreen({commit}, enabled) {
        commit('changeSplashState', enabled)
    }
};

export default {
    state,
    getters,
    mutations,
    actions,
}
